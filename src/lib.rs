#![allow(warnings)]
extern crate mech_core;
extern crate mech_utilities;
extern crate crossbeam_channel;
#[macro_use]
extern crate lazy_static;
extern crate anyhow;
extern crate thiserror;
extern crate regex;
extern crate tch;

pub mod mech_diffusion;
